from django.shortcuts import render, redirect,get_object_or_404
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.contrib.admin.views.decorators import staff_member_required
from django.views.decorators.http import require_POST
from cart.cart import Cart
from shop.models import Product
from coupons.forms import CouponApplyForm
from .models import Order, OrderItem
from .forms import OrderCreateForm

def order_create(request):
	cart = Cart(request)
	if request.method == 'POST':
		data= {}

		form = OrderCreateForm(request.POST)
		if form.is_valid():
			data['error']=False
			order = form.save(commit=False)
			if cart.coupon:
				order.coupon = cart.coupon
				order.discount = cart.coupon.discount
			order.save()
			
			for item in cart:
				OrderItem.objects.create(order=order,
										product=item['product'],
										price=item['price'],
										size=item['size'],
										quantity=item['quantity'])
			cart.clear()
			request.session['coupon_id'] = None
			data['msg_success']= render_to_string("orders/checkout_complete.html")

		else:
			data['error']=True
			data['error_email'] = "Địa chỉ email không hợp lệ" if form['email'].errors else ""
			data['error_phone'] = "Số điện thoại không hợp lệ" if form['phone'].errors else ""

		return JsonResponse(data)

	else:
		form  = OrderCreateForm()


	return render(request, 'orders/checkout.html', {'form':form, 'cart':cart})

@staff_member_required
def manage_orders(request):
	orders = Order.objects.all()

	return render(request, 'orders/manage.html', {'orders':orders})

@staff_member_required
def order_detail(request,order_id):
	order = get_object_or_404(Order, id=order_id)
	form = OrderCreateForm(instance = order)

	return render(request, 'orders/order_detail.html',{'order':order,'form':form})

@staff_member_required
@require_POST
def order_update(request, order_id):
	data= {}
	order = get_object_or_404(Order, id=order_id)
	form = OrderCreateForm(request.POST, instance=order)
	if form.is_valid():
		form.save()
		data['error'] = False

	else:
		data['error']=True
		data['error_email'] = "Địa chỉ email không hợp lệ" if form['email'].errors else ""
		data['error_phone'] = "Số điện thoại không hợp lệ" if form['phone'].errors else ""

	return JsonResponse(data)

@staff_member_required
def order_delete(request, order_id):
	data = {}
	order = get_object_or_404(Order,id=order_id)
	order.delete()
	orders = Order.objects.all()
	data['tbody'] = render_to_string("orders/includes/orders_manage_tbody.html",{'orders':orders})

	return JsonResponse(data)

@staff_member_required
def change_status_paid(request,order_id):
	data = {}
	order= get_object_or_404(Order, id=order_id)
	order.paid = True if order.paid==False else False
	order.save()
	data['status'] = order.paid

	return JsonResponse(data)