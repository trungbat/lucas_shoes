from django import forms
from shop.models import SIZE_CHOICES


PRODUCT_QUANTITY_CHOICES = [(i,str(i)) for i in range(1,21)]

class CartAddProductForm(forms.Form):
	quantity = forms.TypedChoiceField(
		choices=PRODUCT_QUANTITY_CHOICES,
		initial=1,
		coerce=int, label='SL'
	)
	size = forms.TypedChoiceField(
		choices= SIZE_CHOICES,
		initial=36,
		coerce=int
	)
	update_quantity = forms.BooleanField(
		required=False,
		initial=False,
		widget=forms.HiddenInput
	)