from django.shortcuts import render, get_object_or_404
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.utils.text import slugify
from django.views import View
from django.views.decorators.http import require_POST
from django.contrib.admin.views.decorators import staff_member_required
from cart.forms import CartAddProductForm
from .models import Product, Photo
from .forms import ProductFormCreate, PhotoForm

def home(request):
	return render(request, 'home.html', {'section':'home'})
		
def about(request):
	return render(request, 'about.html',{'section':'about'})

def contact(request):
	return render(request, 'contact.html',{'section':'contact'})
	
def product_list(request):
	cart_form = CartAddProductForm()
	products = Product.objects.all()

	return render(request,'shop/list.html', {'products':products,'section':'shop','cart_form':cart_form})


def product_detail(request, slug):
	cart_form = CartAddProductForm()
	product = get_object_or_404(Product, slug=slug)

	return render(request,'shop/detail.html', {'product':product,'section':'shop','cart_form':cart_form})

def cart_detail(request):
	return render(request,'shop/cart_detail.html',{'section':'shop'})

@staff_member_required
def manage_shop(request):
	return render(request, 'shop/manage_shop.html',{'section':'manage'})

@staff_member_required
def upload_product(request):
	form  = ProductFormCreate(initial={"size":["36","37","38","39","40","41",'42',"43","44","45"]})

	return render(request, "shop/upload_product.html", {'form':form,'section':'upload'})

@staff_member_required
@require_POST
def ajax_upload_product(request):
	data = {}
	if request.method == 'POST':
		form = ProductFormCreate(request.POST)
		if form.is_valid():
			form.save()
			data["is_error"] = False
		else:
			data["is_error"] = True
			data['pricve_msg_error'] = "Tối đa 10 chữ số và 3 chữ số sau dấu phẩy" if form['price'].errors else ""

	return JsonResponse(data)

@staff_member_required
@require_POST
def ajax_update_product(request, product_id):
	data = {}
	obj = get_object_or_404(Product, id=product_id)
	
	if request.method == 'POST':
		form = ProductFormCreate(request.POST,instance=obj)
		if form.is_valid():
			form.save()
			data["is_error"] = False
		else:
			data["is_error"] = True
			data['pricve_msg_error'] = "Tối đa 10 chữ số và 3 chữ số sau dấu phẩy" if form['price'].errors else ""

	return JsonResponse(data)

class BasicUploadView(View):
	def post(self, request):
		form = PhotoForm(self.request.POST, self.request.FILES)

		if form.is_valid():
			photo = form.save(commit=False)
			product = Product.objects.first()
			photo.product = product
			photo.save()
			photos= Product.objects.first().photos.all()

			photo_html = render_to_string('shop/includes/product_photos_create.html', {'product':product, 'request':request})
			data = {'is_valid': True, 'name': photo.file.name, 'url': photo.file.url,'photo_html':photo_html}
		else:
			data = {'is_valid': False}

		return JsonResponse(data)

@staff_member_required
def check_name_product(request):
	data = {}
	name = request.GET.get("name")
	slug = slugify(name)
	data['is_error'] = Product.objects.filter(slug=slug).exists()
	data['msg'] = "<a href='/"+slug+"/'> tại đây</a>"

	return JsonResponse(data)

@staff_member_required
def delete_product(request, product_id):
	data= {}
	product = get_object_or_404(Product,pk=product_id)
	product.delete()
	products = Product.objects.all()
	data['product_table_list'] = render_to_string("shop/includes/product_table_list.html",{'products':products,'request':request})

	return JsonResponse(data)

@staff_member_required
def delete_image(request, product_id):
	data= {}
	product = get_object_or_404(Product,pk=product_id)
	if product.photos.exists():
		product.photos.first().delete()

	photo_html = render_to_string('shop/includes/product_photos_create.html', {'product':product, 'request':request, "section":"update"})
	data['photo_html'] = photo_html

	return JsonResponse(data)

@staff_member_required
def upadte_product(request, product_id):
	product = get_object_or_404(Product,pk=product_id)
	form = ProductFormCreate(instance = product)

	return render(request,'shop/update_product.html',{'product':product, 'form':form,"section":"update"})
